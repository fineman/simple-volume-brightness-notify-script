#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit
fi

if [ $1 = "--volume" ]; then
	notify_type=volume
	muted=$(echo $(pactl get-sink-mute @DEFAULT_SINK@) | grep -oE 'yes|no')
	if [ $muted = "yes" ]; then
	name=Volume-Muted
	elif [ $muted = "no" ]; then
	name=Volume
	fi
	value=$(echo $(pactl get-sink-volume @DEFAULT_SINK@) | grep -oP '/ \K[0-9.]+(?=% /)' | head -n1)
elif [ $1 = "--volume-mute" ]; then
	notify_type=volume
	muted=$(echo $(pactl get-sink-mute @DEFAULT_SINK@) | grep -oE 'yes|no')
	if [ $muted = "yes" ]; then
		name=Muted
		value=0
	elif [ $muted = "no" ]; then
		name=Unmuted
		value=$(echo $(pactl get-sink-volume @DEFAULT_SINK@) | grep -oP '/ \K[0-9.]+(?=% /)' | head -n1)
	fi
elif [ $1 = "--brightness" ]; then
	notify_type=brightness
	name=Brightness
	value=$(brightnessctl -m  | awk -F, '{print substr($4, 0, length($4)-1)}')
else
	echo "Check arguments"
	exit
fi

notify () {
	notify-send -a progressbar_notify -e -h string:x-canonical-private-synchronous:${notify_type} \
		-t 1000 -h int:value:$2 "$1: $2%"
}

notify $name $value 
