# Achtung - archive
I moved it do my [dotfiles repo](https://github.com/finem4n/dotfiles)

# Description
This is simple bash script to send a notification with progressbar on Volume/Brightness change. Works well with sway and waybar

# Depedencies
 - notify-send
 - notification daemon with progressbar, e.g. mako, swaync
 - pactl
 - brigthnessctl

# Usage
## Sway
Put this lines in sway config:

```
# Volume Up
        bindsym XF86AudioRaiseVolume    exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +1% && $HOME/.scripts/progressbar.sh --volume
# Volume Down
        bindsym XF86AudioLowerVolume    exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -1% && $HOME/.scripts/progressbar.sh --volume
# Mute
        bindsym XF86AudioMute           exec --no-startup-id pactl set-sink-mute 0 toggle && $HOME/.scripts/progressbar.sh --volume-mute

# Brightness Up
        bindsym XF86MonBrightnessUp     exec brightnessctl set 1%+ && $HOME/.scripts/progressbar.sh --brightness 
# Brightness Down
        bindsym XF86MonBrightnessDown   exec brightnessctl set 1%- && $HOME/.scripts/progressbar.sh --brightness
```

## Waybar
In pulseaudio module:
```
"on-click-middle": "pactl set-sink-mute 0 toggle && $HOME/.scripts/progressbar.sh --volume-mute",
"on-scroll-up": "pactl set-sink-volume @DEFAULT_SINK@ +1% && $HOME/.scripts/progressbar.sh --volume",
"on-scroll-down": "pactl set-sink-volume @DEFAULT_SINK@ -1% && $HOME/.scripts/progressbar.sh --volume"
```

In backlight module:
```
"on-scroll-down":	"brightnessctl set 1%- && $HOME/.scripts/progressbar.sh --brightness",
"on-scroll-up":		"brightnessctl set +1% && $HOME/.scripts/progressbar.sh --brightness",
```